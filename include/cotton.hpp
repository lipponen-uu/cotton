/*
Free Public License 1.0.0

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN 
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
OF THIS SOFTWARE.
*/

// cotton.hpp

#pragma once
#ifndef COTTON_HPP_INCLUDED
#define COTTON_HPP_INCLUDED

#include <string>
#include <vector>
#include <list>

#define KiloByte(x) (x*1024UL)
#define MegaByte(x) (KiloByte(x)*1024UL)
#define GigaByte(x) (MegaByte(x)*1024UL)
#define TeraByte(x) (GigaByte(x)*1024UL)
#define ArraySize(x) (sizeof(x)/sizeof(x[0]))
#define DiffPtr(x,y) ((uintptr_t)y - (uintptr_t)x)

#define CLASS_RTTI_DECL(_class) static unsigned get_type() const
#define CLASS_RTTI_DEF (_class) unsigned _class::get_type() const { return rtti::type_id<_class>(); }

namespace cotton
{
  namespace utility
  {
    std::string to_string(const char* fmt, ...);
    unsigned to_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
  }

  class Math
  {
  public:
    static const float PI;
    static float sin(float degrees);
    static float cos(float degrees);
    static float acos(float degrees);
    static float atan2(float y, float x);
    static float sqrt(float value);
    static float random();
    static float random(float min, float max);
    static unsigned hash(const void* buffer, const unsigned length); // murmurhash3 32bit
    
    template <class T>
    static T min(T a, T b)
    {
      return a < b ? a : b;
    }

    template <class T>
    static T max(T a, T b)
    {
      return a > b ? a : b;
    }

    template <class T>
    static T abs(T v)
    {
      return v < 0 ? -v : v;
    }

    template <class T>
    static T clamp(T a, T v, T w)
    {
      return a < v ? v : a > w ? w : a;
    }
  };

  class Vector2
  {
  public:
    Vector2();
    Vector2(const Vector2& rhs);
    Vector2(const float x, const float y);
    explicit Vector2(const int x, const int y);
    
    Vector2 operator-() const;
    Vector2 operator+(const Vector2& rhs) const;
    Vector2 operator-(const Vector2& rhs) const;
    Vector2 operator*(const float rhs) const;
    Vector2 operator/(const float rhs) const;
    Vector2& operator=(const Vector2& rhs);
    Vector2& operator+=(const Vector2& rhs);
    Vector2& operator-=(const Vector2& rhs);
    Vector2& operator*=(const float rhs);
    Vector2& operator/=(const float rhs);

    float m_x, m_y;
  };

  void normalize(Vector2& rhs);
  float length(const Vector2& rhs);
  float squared_length(const Vector2& rhs);
  float dot(const Vector2& lhs, const Vector2& rhs);
  float distance(const Vector2& lhs, const Vector2& rhs);
  float to_angle(const Vector2& rhs); // note(tommi): needs to be a unit vector
  Vector2 normalized(const Vector2& rhs);
  Vector2 interpolate_linear(const Vector2& lhs, const Vector2& rhs, float t);
  Vector2 interpolate_spherical(const Vector2& lhs, const Vector2& rhs, float t);

  class Rect
  {
  public:
    Rect();
    Rect(int x, int y, int w, int h);
    Rect(const Rect& rhs);
    Rect& operator=(const Rect& rhs);

    Rect operator+(const Rect& rhs)
    {
      return Rect(m_x + rhs.m_x, m_y + rhs.m_y, m_w + rhs.m_w, m_h + rhs.m_h);
    }

    Rect operator-(const Rect& rhs)
    {
      return Rect(m_x - rhs.m_x, m_y - rhs.m_y, m_w - rhs.m_w, m_h - rhs.m_h);
    }

    Rect operator+(const Vector2& rhs)
    {
      Rect result;
      result.m_x = m_x + (int)rhs.m_x;
      result.m_y = m_y + (int)rhs.m_y;
      result.m_w = m_w + (int)rhs.m_x;
      result.m_h = m_h + (int)rhs.m_y;
      return result;
    }

    int m_x, m_y, m_w, m_h;
  };

  class Time
  {
  public:
    Time();
    Time(const Time& rhs);

    Time operator-(const Time& rhs);
    Time operator+(const Time& rhs);
    Time& operator=(const Time& rhs);

    float as_seconds() const;
    int as_milliseconds() const;
    __int64 as_microseconds() const;

  private:
    explicit Time(__int64 microseconds);

  private:
    friend class Clock;
    __int64 m_microseconds;
  };

  class Clock
  {
  public:
    static Time get_current_time();

    Clock();

    Time get_elapsed_time();
    void reset();

  private:
    Time m_start;
  };

  enum KeyCode
  {
    kKeyCode_Space,
    kKeyCode_Escape,
    kKeyCode_Enter,
    kKeyCode_Backspace,
    kKeyCode_F1,
    kKeyCode_F2,
    kKeyCode_F3,
    kKeyCode_F4,
    kKeyCode_F5,
    kKeyCode_F6,
    kKeyCode_F7,
    kKeyCode_F8,
    kKeyCode_F9,
    kKeyCode_F10,
    kKeyCode_F11,
    kKeyCode_F12,
    kKeyCode_Insert,
    kKeyCode_Home,
    kKeyCode_PageUp,
    kKeyCode_Delete,
    kKeyCode_End,
    kKeyCode_PageDown,
    kKeyCode_Up,
    kKeyCode_Left,
    kKeyCode_Down,
    kKeyCode_Right,
    kKeyCode_Tab,
    kKeyCode_CapsLock,
    kKeyCode_Shift,
    kKeyCode_Control,
    kKeyCode_Alt,
    kKeyCode_NumPadNumLock,
    kKeyCode_Divide,
    kKeyCode_Multiply,
    kKeyCode_Minus,
    kKeyCode_Plus,
    kKeyCode_NumPad0,
    kKeyCode_NumPad1,
    kKeyCode_NumPad2,
    kKeyCode_NumPad3,
    kKeyCode_NumPad4,
    kKeyCode_NumPad5,
    kKeyCode_NumPad6,
    kKeyCode_NumPad7,
    kKeyCode_NumPad8,
    kKeyCode_NumPad9,
    kKeyCode_NumPadComma,
    kKeyCode_Comma, 
    kKeyCode_Period,
    kKeyCode_UnderScore,
    kKeyCode_LessThan,
    kKeyCode_0,
    kKeyCode_1,
    kKeyCode_2,
    kKeyCode_3,
    kKeyCode_4,
    kKeyCode_5,
    kKeyCode_6,
    kKeyCode_7,
    kKeyCode_8,
    kKeyCode_9,
    kKeyCode_A,
    kKeyCode_B,
    kKeyCode_C,
    kKeyCode_D,
    kKeyCode_E,
    kKeyCode_F,
    kKeyCode_G,
    kKeyCode_H,
    kKeyCode_I,
    kKeyCode_J,
    kKeyCode_K,
    kKeyCode_L,
    kKeyCode_M,
    kKeyCode_N,
    kKeyCode_O,
    kKeyCode_P,
    kKeyCode_Q,
    kKeyCode_R,
    kKeyCode_S,
    kKeyCode_T,
    kKeyCode_U,
    kKeyCode_V,
    kKeyCode_W,
    kKeyCode_X,
    kKeyCode_Y,
    kKeyCode_Z,
    kKeyCode_Invalid,
  };

  enum MouseButton
  {
    kMouseButton_Left,
    kMouseButton_Right,
    kMouseButton_Invalid,
  };

  enum WindowEventType
  {
    kWindowEventType_MouseMove,
    kWindowEventType_MouseDown,
    kWindowEventType_MouseUp,
    kWindowEventType_KeyDown,
    kWindowEventType_KeyUp,
    kWindowEventType_Text,
    kWindowEventType_Quit,
    kWindowEventType_Invalid,
  };

  class WindowEvent
  {
  public:
    WindowEvent();
    WindowEvent(const WindowEvent& rhs);
    WindowEvent& operator=(const WindowEvent& rhs);

    WindowEventType get_type() const;
    float get_x() const;
    float get_y() const;
    MouseButton get_button() const;
    KeyCode get_keycode() const;
    unsigned get_unicode() const;

  private:
    friend class EventFactory;
    WindowEventType m_type;
    float m_x;
    float m_y;
    MouseButton m_button;
    KeyCode m_keycode;
    unsigned m_unicode;
  };

  class Config
  {
  public:
    Config(int argc, char** argv);
    ~Config();

    int m_width;
    int m_height;
    std::string m_path;
  };

  class Vertex
  {
  public:
    Vertex();
    Vertex(const Vector2& position, const Vector2& texcoord, const unsigned color);

    Vector2 m_position;
    Vector2 m_texcoord;
    unsigned m_color;
  };

  class Transform
  {
  public:
    Transform();

    void set_origin(const Vector2& origin);
    void set_origin(float x, float y);
    void set_position(const Vector2& position);
    void set_position(float x, float y);
    void set_scale(const Vector2& scale);
    void set_scale(float x, float y);
    void set_rotation(float rotation);
    const Vector2& get_origin() const;
    const Vector2 get_position() const;
    const Vector2& get_scale() const;
    float get_rotation() const;

  private:
    Vector2 m_origin;
    Vector2 m_position;
    Vector2 m_scale;
    float m_rotation;
  };

  class Texture
  {
  public:
    // note(tommi): png only
    Texture();
    ~Texture();

    bool create_from_filename(const char* filename);
    bool create_from_memory(int width, int height, const void* bitmap);
    void destroy();
    bool is_valid() const;
    int get_width() const;
    int get_height() const;
    unsigned get_handle() const;

  private:
    unsigned m_id;
    int m_width;
    int m_height;
  };

  class Window
  {
    // note(tommi): non-copyable
    Window(const Window&) { }
    Window& operator=(const Window&) { return *this; }

  public:
    Window();
    ~Window();

    bool open(int width, int height, const char* title);
    void close();
    bool is_open() const;
    void set_title(const char* title);
    int get_width() const;
    int get_height() const;
    void* get_native_handle() const;
    bool poll_event(WindowEvent& event);

    void clear();
    void present();
    void draw(const Transform& transform, const Vertex* vertices, const unsigned num_vertices, const class Texture* texture) const;
    void set_clip(const Rect& clip) const;
    void reset_clip() const;

  private:
    void push_event(const WindowEvent& event);

  private:
    friend class EventFactory;
    void* m_handle;
    void* m_context;
    int m_width;
    int m_height;
    std::list<WindowEvent> m_queue;
  };

  class Font
  {
    // note(tommi): non-copyable
    Font(const Font&) { }
    Font& operator=(const Font&) { return *this; }

  public:
    Font();
    ~Font();

    bool create_from_filename(const char* filename, float size);
    void destroy();
    void draw(const Window& window, int x, int y, unsigned color, const std::string& text);

  private:
    struct Glyph
    {
      float width;
      float height;
      float offset_x;
      float offset_y;
      float uv[4];
    };
    friend void fill(std::vector<Vertex>& vertices, unsigned& offset, const Glyph& glyph, unsigned color, float x, float y);

    std::vector<Glyph> m_glyphs;
    std::vector<Vertex> m_vertices;
    float m_size;
    Texture m_texture;
  };

  class Audio
  {
    // note(tommi): non-copyable
    Audio(const Audio&) {}
    Audio& operator=(const Audio&) { return *this; }

  public:
    Audio();
    ~Audio();

    bool open(const Window& window);
    void close();
    void present();

  private:
    friend class AudioFactory;
    friend class AudioBufferFactory;
    friend class AudioPlayer;
    class Impl;
    Impl* m_pimpl;
  };

  class Sound
  {
  public:
    // note(tommi): ogg vorbis 44100hz
    Sound(Audio& audio, const char* filename);
    ~Sound();

    void destroy();
    void play(float volume);

  private:
    friend class AudioFactory;
    friend class AudioPlayer;
    class Impl;
    Impl* m_pimpl;
  };

  class Music
  {
  public:
    // note(tommi): ogg vorbis 44100hz
    Music(Audio& audio, const char* filename);
    ~Music();

    void destroy();
    void update();
    void play(float volume);
    void pause();
    void stop();
    bool is_playing() const;
    void set_volume(float volume);

  private:
    friend class AudioFactory;
    friend class AudioPlayer;
    class Impl;
    Impl* m_pimpl;
  };

  // note(tommi): below is all game related classes
  // there might be too many and be extracted into
  // their own file eventually
  class Sprite
  {
  public:
    Sprite();
    Sprite(const Texture* texture);
    Sprite(const Texture* texture, const Rect& texture_rectangle);
    ~Sprite();

    void set_texture(const Texture* texture);
    void set_rectangle(const Rect& rect);
    void set_rectangle(int x, int y, int w, int h);
    void set_color(unsigned color);

    unsigned get_vertex_count() const;
    const Vertex* get_vertices() const;
    const Texture* get_texture() const;

  private:
    void update(const Rect& rect);

  private:
    const Texture* m_texture;
    Vertex m_vertices[4];
  };

  class SpriteBatch
  {
  public:
    SpriteBatch();
    SpriteBatch(const Texture* texture);
    SpriteBatch(const Texture* texture, unsigned num_vertices);
    ~SpriteBatch();

    void set_texture(const Texture* texture);

    bool is_valid() const;
    unsigned get_vertex_count() const;
    const Vertex* get_vertices() const;
    const Texture* get_texture() const;

    void push(const Rect& position, const Rect& texcoord, unsigned color = 0xffffffff);
    void reset();

  private:
    const Texture* m_texture;
    std::vector<Vertex> m_vertices;
  };

  class Entity
  {
    // note(tommi): non-copyable
    Entity(const Entity&) { }
    Entity& operator=(const Entity&) { }

  public:
    Entity() { }
    virtual ~Entity() { }
    virtual void update(float deltatime);
    virtual void draw(const Window& window);

    void set_layer_bits(unsigned layer_bits);
    unsigned get_layer_bits() const;
    const Transform& get_transform() const;
    Transform& get_transform();
    const Sprite& get_sprite() const;
    Sprite& get_sprite();
    Vector2 get_dimensions() const;

  protected:
    unsigned m_layer_bits;
    Transform m_transform;
    Sprite m_sprite;
  };

  typedef std::vector<const Entity*> QueryResult;

  class EntitySystem
  {
    // note(tommi): non-copyable
    EntitySystem(const EntitySystem&) { }
    EntitySystem& operator=(const EntitySystem&) { return *this; }

  public:
    EntitySystem();
    ~EntitySystem();

    void update(float deltatime);
    void draw(const Window& window);

    void attach_entity(Entity* entity);
    void detach_entity(Entity* entity);

    unsigned query_in_range(QueryResult& result, const Vector2& center,
      const float range, const unsigned layer) const;

  private:
    typedef std::vector<Entity*> EntityArray;
    EntityArray m_entities;
  };

  class Camera
  {
  public:
    Camera(const Window& window);
    Camera(const Window& window, const Rect& bounds);
    Camera(const Window& window, const Rect& bounds, const Vector2& position);
    Camera(const Window& window, const Vector2& position);

    void set_bounds(const Rect& limit);
    const Rect& get_bounds() const;

    void move(const Vector2& amount);
    void set_position(const Vector2& position);
    const Vector2& get_position() const;

    Transform transform(const Transform& t) const;

  private:
    void check_bounds();

  private:
    const Window& m_window;
    Rect m_bounds;
    Vector2 m_position;
  };

  class Keyboard
  {
  public:
	  Keyboard()
	  {
		  memset(m_current, 0, sizeof(m_current));
		  memset(m_previous, 0, sizeof(m_previous));
	  }

	  bool is_down(KeyCode keycode) const { return m_current[keycode]; }
	  bool is_down_once(KeyCode keycode) const { return m_current[keycode] && !m_previous[keycode]; }
	  void update() { memcpy(m_previous, m_current, sizeof(m_previous)); }
	  void on_event(const WindowEvent& event)
	  {
		  switch (event.get_type())
		  {
		  case kWindowEventType_KeyDown:
		  {
			  if (!m_current[event.get_keycode()])
			  {
				  m_current[event.get_keycode()] = true;
			  }
		  } break;
		  case  kWindowEventType_KeyUp:
		  {
			  m_current[event.get_keycode()] = false;
		  } break;
		  }
	  }

  private:
	  bool m_current[kKeyCode_Invalid];
	  bool m_previous[kKeyCode_Invalid];
  };

  class Mouse
  {
  public:
	  Mouse()
	  {
		  memset(m_current, 0, sizeof(m_current));
		  memset(m_previous, 0, sizeof(m_previous));
	  }

	  bool is_down(MouseButton button) const { return m_current[button]; }
	  bool is_down_once(MouseButton button) const { return m_current[button] && !m_previous[button]; }
	  void update() { memcpy(m_previous, m_current, sizeof(m_previous)); m_cursor_previous = m_cursor_current; }
	  Vector2 get_cursor() const { return m_cursor_current; }
	  Vector2 get_cursor_delta() const { return m_cursor_current - m_cursor_previous; }
	  void on_event(const WindowEvent& event)
	  {
		  switch (event.get_type())
		  {
		  case kWindowEventType_MouseMove:
		  {
			  m_cursor_current = Vector2(event.get_x(), event.get_y());
		  } break;
		  case kWindowEventType_MouseDown:
		  {
			  m_current[event.get_button()] = true;
		  } break;
		  case kWindowEventType_MouseUp:
		  {
			  m_current[event.get_button()] = false;
		  } break;
		  }
	  }

  private:
	  bool m_current[kMouseButton_Invalid];
	  bool m_previous[kMouseButton_Invalid];
	  Vector2 m_cursor_current;
	  Vector2 m_cursor_previous;
  };

  namespace gui
  {
    void initialize(const Window& window, const Texture* texture);
    void on_event(const WindowEvent& event);
    void prepare();
    void flush();
    void label(int id, int x, int y, const char* text);
    bool button(int id, int x, int y, int width, int height, const char* text);
  }
}

#endif // COTTON_HPP_INCLUDED
