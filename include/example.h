#pragma once

#include "cotton.hpp"
using namespace cotton;
// Example creating a terrain
class Terrain
{
public:
	Terrain(const Texture* texture, unsigned width, unsigned height)
		: m_batch(texture)
	{
		const int num_widths = 1;
		const int num_heights = 2;

		int pw = 64, ph = 64;
		int num_tiles_x = (width / pw + 1) * num_widths;
		int num_tiles_y = (height / ph + 1) * num_heights;

		Rect texs[] =
		{
			Rect(1, 1, 30, 30),
			Rect(33, 1, 30, 30),
			Rect(65, 1, 30, 30),
		};

		for (int y = 0; y < num_tiles_y; y++)
		{
			int py = y * ph;
			for (int x = 0; x < num_tiles_x; x++)
			{
				int px = x * pw;
				unsigned type = ((unsigned)Math::random(0.0f, (float)ArraySize(texs))) % ArraySize(texs);
				Rect pos(px, py, pw, ph);
				m_batch.push(pos, texs[type]);
			}
		}
	}

	void draw(const Camera& camera, const Window& window)
	{
		window.draw(camera.transform(m_transform), m_batch.get_vertices(), m_batch.get_vertex_count(), m_batch.get_texture());
	}

private:
	Transform m_transform;
	SpriteBatch m_batch;
};



// Example creating a a specific Entity
class Boid : public Entity
{
public:
	Boid(Camera* camera, const Texture* texture, const int x, const int y, const int w, const int h, const float pos_x, const float pos_y)
	{
		m_camera = camera;
		m_sprite.set_texture(texture);
		m_sprite.set_rectangle(x, y, w, h);
		m_transform.set_position(pos_x, pos_y);
		m_transform.set_scale(0.5f, 0.5f);

	}
	~Boid()
	{

	}
	void draw(const Window& window)
	{
		window.draw(m_camera->transform(m_transform), m_sprite.get_vertices(), m_sprite.get_vertex_count(), m_sprite.get_texture());
	}
private:
	Camera* m_camera = nullptr;
	Boid() {};
};