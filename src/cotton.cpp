/*
Free Public License 1.0.0

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
OF THIS SOFTWARE.
*/

// cotton.cpp

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")

#define _CRT_SECURE_NO_WARNINGS 1
#define NOMINMAX 1

#include <WinSock2.h>
#include <Windows.h>
#include <windowsx.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <dsound.h>
#include <cassert>
#include <cstdarg>
#include <cmath>
#include <climits>
#include <random>
#include <cotton.hpp>

#pragma warning(push)
#pragma warning(disable:4100)
#pragma warning(disable:4189)
#pragma warning(disable:4244)
#pragma warning(disable:4245)
#pragma warning(disable:4456)
#pragma warning(disable:4457)
#pragma warning(disable:4505)
#pragma warning(disable:4701)
#include <stb_vorbis.h>
#define STB_RECT_PACK_IMPLEMENTATION 1
#define STBRP_STATIC 1
#include <stb_rect_pack.h>
#define STB_IMAGE_IMPLEMENTATION 1
#define STBI_ONLY_PNG 1
#define STBI_NO_SIMD 1
#include <stb_image.h>
#define STB_TRUETYPE_IMPLEMENTATION 1
#define STBTT_STATIC 1
#include <stb_truetype.h>
#pragma warning(pop)

namespace cotton
{
  namespace utility
  {
    std::string to_string(const char* fmt, ...)
    {
      char result[2048] = { 0 };
      va_list vl;
      va_start(vl, fmt);
      vsprintf_s<2048>(result, fmt, vl);
      va_end(vl);
      return std::string(result);
    }

    unsigned to_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
    {
      unsigned result = 0;
      result |= (r << 0);
      result |= (g << 8);
      result |= (b << 16);
      result |= (a << 24);
      return result;
    }
  }

  const float Math::PI = 3.141592f;

  float Math::sin(float degrees)
  {
    return sinf(degrees);
  }

  float Math::cos(float degrees)
  {
    return cosf(degrees);
  }

  float Math::acos(float degrees)
  {
    return acosf(degrees);
  }

  float Math::atan2(float y, float x)
  {
    return atan2f(y, x);
  }

  float Math::sqrt(float value)
  {
    return sqrtf(value);
  }

  float Math::random()
  {
    static std::random_device device;
    static std::mt19937 mt(device());
    static std::uniform_real_distribution<float> distrubution(0.0f, 1.0f);
    return distrubution(mt);
  }

  float Math::random(float min, float max)
  {
    return min + (max - min) * random();
  }

  // raw.githubusercontent.com/chaoslawful/ccard-lib/master/src/murmurhash.c
  unsigned Math::hash(const void* buffer, const unsigned length)
  {
    unsigned char *data = (unsigned char*)buffer;
    unsigned m = 0x5bd1e995;
    unsigned r = 24;
    unsigned h = 0xe17a1465UL ^ length;
    unsigned len_4 = length >> 2;
    unsigned i;
    unsigned len_m;
    unsigned left;

    for (i = 0; i < len_4; i++) {
      unsigned i_4 = i << 2;
      unsigned k = data[i_4 + 3];
      k <<= 8;
      k |= data[i_4 + 2] & 0xff;
      k <<= 8;
      k |= data[i_4 + 1] & 0xff;
      k <<= 8;
      k |= data[i_4 + 0] & 0xff;
      k *= m;
      k ^= k >> r;
      k *= m;
      h *= m;
      h ^= k;
    }

    // avoid calculating modulo
    len_m = len_4 << 2;
    left = length - len_m;

    if (left != 0) {
      if (left >= 3) {
        h ^= data[length - 3] << 16;
      }
      if (left >= 2) {
        h ^= data[length - 2] << 8;
      }
      if (left >= 1) {
        h ^= data[length - 1];
      }
      h *= m;
    }
    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;
    return h;
  }

  Vector2::Vector2()
  {
    m_x = 0.0f;
    m_y = 0.0f;
  }

  Vector2::Vector2(const Vector2& rhs)
  {
    m_x = rhs.m_x;
    m_y = rhs.m_y;
  }

  Vector2::Vector2(const float x, const float y)
  {
    m_x = x;
    m_y = y;
  }

  Vector2::Vector2(const int x, const int y)
  {
    m_x = (float)x;
    m_y = (float)y;
  }

  Vector2 Vector2::operator-() const
  {
    return Vector2(-m_x, -m_y);
  }

  Vector2 Vector2::operator+(const Vector2& rhs) const
  {
    return Vector2(m_x + rhs.m_x, m_y + rhs.m_y);
  }

  Vector2 Vector2::operator-(const Vector2& rhs) const
  {
    return Vector2(m_x - rhs.m_x, m_y - rhs.m_y);
  }

  Vector2 Vector2::operator*(const float rhs) const
  {
    return Vector2(m_x * rhs, m_y * rhs);
  }

  Vector2 Vector2::operator/(const float rhs) const
  {
    return Vector2(m_x / rhs, m_y / rhs);
  }

  Vector2& Vector2::operator=(const Vector2& rhs)
  {
    m_x = rhs.m_x;
    m_y = rhs.m_y;
    return *this;
  }

  Vector2& Vector2::operator+=(const Vector2& rhs)
  {
    m_x += rhs.m_x;
    m_y += rhs.m_y;
    return *this;
  }

  Vector2& Vector2::operator-=(const Vector2& rhs)
  {
    m_x -= rhs.m_x;
    m_y -= rhs.m_y;
    return *this;
  }

  Vector2& Vector2::operator*=(const float rhs)
  {
    m_x *= rhs;
    m_y *= rhs;
    return *this;
  }

  Vector2& Vector2::operator/=(const float rhs)
  {
    m_x /= rhs;
    m_y /= rhs;
    return *this;
  }

  float length(const Vector2& rhs)
  {
    return Math::sqrt(rhs.m_x * rhs.m_x + rhs.m_y * rhs.m_y);
  }

  float squared_length(const Vector2& rhs)
  {
    return rhs.m_x * rhs.m_x + rhs.m_y * rhs.m_y;
  }

  float dot(const Vector2& lhs, const Vector2& rhs)
  {
    return lhs.m_x * rhs.m_x + lhs.m_y * rhs.m_y;
  }

  void normalize(Vector2& rhs)
  {
    float len = length(rhs);
    if (len > 0.0f)
    {
      rhs /= len;
    }
  }

  Vector2 normalized(const Vector2& rhs)
  {
    Vector2 result(rhs);
    normalize(result);
    return result;
  }

  float distance(const Vector2& lhs, const Vector2& rhs)
  {
    Vector2 diff = rhs - lhs;
    return length(diff);
  }

  float to_angle(const Vector2& rhs)
  {
    return Math::atan2(rhs.m_y, rhs.m_x) * 180.0f / Math::PI;
  }

  Vector2 interpolate_linear(const Vector2& lhs, const Vector2& rhs, float t)
  {
    Vector2 result;
    result.m_x = lhs.m_x * (1.0f - t) + rhs.m_x * t;
    result.m_y = lhs.m_y * (1.0f - t) + rhs.m_y * t;
    return result;
  }

  Vector2 interpolate_spherical(const Vector2& lhs, const Vector2& rhs, float t)
  {
    float d = dot(lhs, rhs);
    d = Math::clamp(d, -1.0f, 1.0f);
    float theta = Math::acos(d) * t;
    Vector2 r = normalized(rhs - lhs * d);
    Vector2 result = (rhs * Math::cos(theta)) + (r * Math::sin(theta));
    return result;
  }

  Rect::Rect()
  {
    m_x = 0;
    m_y = 0;
    m_w = 0;
    m_h = 0;
  }

  Rect::Rect(int x, int y, int w, int h)
  {
    m_x = x;
    m_y = y;
    m_w = w;
    m_h = h;
  }

  Rect::Rect(const Rect& rhs)
  {
    m_x = rhs.m_x;
    m_y = rhs.m_y;
    m_w = rhs.m_w;
    m_h = rhs.m_h;
  }

  Rect& Rect::operator=(const Rect& rhs)
  {
    m_x = rhs.m_x;
    m_y = rhs.m_y;
    m_w = rhs.m_w;
    m_h = rhs.m_h;
    return *this;
  }

  Time::Time()
  {
    m_microseconds = 0;
  }

  Time::Time(__int64 microseconds)
  {
    m_microseconds = microseconds;
  }

  Time::Time(const Time& rhs)
  {
    m_microseconds = rhs.m_microseconds;
  }

  Time Time::operator-(const Time& rhs)
  {
    return Time(m_microseconds - rhs.m_microseconds);
  }

  Time Time::operator+(const Time& rhs)
  {
    return Time(m_microseconds + rhs.m_microseconds);
  }

  Time& Time::operator=(const Time& rhs)
  {
    m_microseconds = rhs.m_microseconds;
    return *this;
  }

  float Time::as_seconds() const
  {
    return (float)(m_microseconds / 1000000.0);
  }

  int Time::as_milliseconds() const
  {
    return (int)(m_microseconds / 1000);
  }

  __int64 Time::as_microseconds() const
  {
    return m_microseconds;
  }

  Time Clock::get_current_time()
  {
    static __int64 frequency = 0;
    if (!frequency) QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
    __int64 now = 0;
    QueryPerformanceCounter((LARGE_INTEGER*)&now);
    return Time(1000000 * now / frequency);
  }

  Clock::Clock()
  {
    m_start = get_current_time();
  }

  Time Clock::get_elapsed_time()
  {
    return get_current_time() - m_start;
  }

  void Clock::reset()
  {
    m_start = get_current_time();
  }

  WindowEvent::WindowEvent()
  {
    m_type = kWindowEventType_Invalid;
  }
  
  WindowEvent::WindowEvent(const WindowEvent& rhs)
  {
    m_type = rhs.m_type;
    m_x = rhs.m_x;
    m_y = rhs.m_y;
    m_button = rhs.m_button;
    m_keycode = rhs.m_keycode;
    m_unicode = rhs.m_unicode;
  }
  
  WindowEvent& WindowEvent::operator=(const WindowEvent& rhs)
  {
    m_type = rhs.m_type;
    m_x = rhs.m_x;
    m_y = rhs.m_y;
    m_button = rhs.m_button;
    m_keycode = rhs.m_keycode;
    m_unicode = rhs.m_unicode;
    return *this;
  }

  WindowEventType WindowEvent::get_type() const
  {
    return m_type;
  }

  float WindowEvent::get_x() const
  {
    return m_x;
  }

  float WindowEvent::get_y() const
  {
    return m_y;
  }

  MouseButton WindowEvent::get_button() const
  {
    return m_button;
  }

  KeyCode WindowEvent::get_keycode() const
  {
    return m_keycode;
  }

  unsigned WindowEvent::get_unicode() const
  {
    return m_unicode;
  }
  
  class EventFactory
  {
  public:
    static void push(Window& window, WindowEventType type)
    {
      WindowEvent result;
      result.m_type = type;
      window.push_event(result);
    }

    static void push(Window& window, WindowEventType type, float x, float y)
    {
      WindowEvent result;
      result.m_type = type;
      result.m_x = x;
      result.m_y = y;
      window.push_event(result);
    }

    static void push(Window& window, WindowEventType type, MouseButton button)
    {
      WindowEvent result;
      result.m_type = type;
      result.m_button = button;
      window.push_event(result);
    }

    static void push(Window& window, WindowEventType type, KeyCode keycode)
    {
      WindowEvent result;
      result.m_type = type;
      result.m_keycode = keycode;
      window.push_event(result);
    }

    static void push(Window& window, WindowEventType type, unsigned unicode)
    {
      WindowEvent result;
      result.m_type = type;
      result.m_unicode = unicode;
      window.push_event(result);
    }
  };

  Config::Config(int argc, char** argv)
    : m_width(1280)
    , m_height(720)
  {
    m_path = argv[0];
    for (unsigned i = 0; i < m_path.length(); i++)
    {
      if (m_path[i] == '\\')
        m_path[i] = '/';
    }
    auto pos = m_path.find_last_of('/') + 1;
    if(pos != std::string::npos)
      m_path = m_path.substr(0, pos);
    if (argc > 1)
    {
      for (int i = 1; i < argc; i++)
      {
        const char* param = argv[i];
        if (strcmp(param, "-w") == 0)
        {
        }
        else if (strcmp(param, "-h") == 0)
        {
        }
      }
    }
  }

  Config::~Config()
  {
    m_path.clear();
  }

  static KeyCode convert(WPARAM wParam)
  {
    switch (wParam)
    {
      case VK_SPACE: return kKeyCode_Space;
      case VK_ESCAPE: return kKeyCode_Escape;
      case VK_RETURN: return kKeyCode_Enter;
      case VK_BACK: return kKeyCode_Backspace;
      case VK_F1: return kKeyCode_F1;
      case VK_F2: return kKeyCode_F2;
      case VK_F3: return kKeyCode_F3;
      case VK_F4: return kKeyCode_F4;
      case VK_F5: return kKeyCode_F5;
      case VK_F6: return kKeyCode_F6;
      case VK_F7: return kKeyCode_F7;
      case VK_F8: return kKeyCode_F8;
      case VK_F9: return kKeyCode_F9;
      case VK_F10: return kKeyCode_F10;
      case VK_F11: return kKeyCode_F11;
      case VK_F12: return kKeyCode_F12;
      case VK_INSERT: return kKeyCode_Insert;
      case VK_HOME: return kKeyCode_Home;
      case VK_PRIOR: return kKeyCode_PageUp;
      case VK_DELETE: return kKeyCode_Delete;
      case VK_NEXT: return kKeyCode_PageDown;
      case VK_UP: return kKeyCode_Up;
      case VK_LEFT: return kKeyCode_Left;
      case VK_DOWN: return kKeyCode_Down;
      case VK_RIGHT: return kKeyCode_Right;
      case VK_TAB: return kKeyCode_Tab;
      case VK_CAPITAL: return kKeyCode_CapsLock;
      case VK_SHIFT: return kKeyCode_Shift;
      case VK_CONTROL: return kKeyCode_Control;
      case VK_MENU: return kKeyCode_Alt;
      case VK_NUMLOCK: return kKeyCode_NumPadNumLock;
      case VK_DIVIDE: return kKeyCode_Divide;
      case VK_MULTIPLY: return kKeyCode_Multiply;
      case VK_SUBTRACT: return kKeyCode_Minus;
      case VK_ADD: return kKeyCode_Plus;
      case VK_NUMPAD0: return kKeyCode_NumPad0;
      case VK_NUMPAD1: return kKeyCode_NumPad1;
      case VK_NUMPAD2: return kKeyCode_NumPad2;
      case VK_NUMPAD3: return kKeyCode_NumPad3;
      case VK_NUMPAD4: return kKeyCode_NumPad4;
      case VK_NUMPAD5: return kKeyCode_NumPad5;
      case VK_NUMPAD6: return kKeyCode_NumPad6;
      case VK_NUMPAD7: return kKeyCode_NumPad7;
      case VK_NUMPAD8: return kKeyCode_NumPad8;
      case VK_NUMPAD9: return kKeyCode_NumPad9;
      case VK_DECIMAL: return kKeyCode_NumPadComma;
      case VK_END: return kKeyCode_Comma;
      case VK_OEM_PERIOD: return kKeyCode_Period;
      case VK_OEM_MINUS: return kKeyCode_UnderScore;
      case '0': return kKeyCode_0;
      case '1': return kKeyCode_1;
      case '2': return kKeyCode_2;
      case '3': return kKeyCode_3;
      case '4': return kKeyCode_4;
      case '5': return kKeyCode_5;
      case '6': return kKeyCode_6;
      case '7': return kKeyCode_7;
      case '8': return kKeyCode_8;
      case '9': return kKeyCode_9;
      case 'A': return kKeyCode_A;
      case 'B': return kKeyCode_B;
      case 'C': return kKeyCode_C;
      case 'D': return kKeyCode_D;
      case 'E': return kKeyCode_E;
      case 'F': return kKeyCode_F;
      case 'G': return kKeyCode_G;
      case 'H': return kKeyCode_H;
      case 'I': return kKeyCode_I;
      case 'J': return kKeyCode_J;
      case 'K': return kKeyCode_K;
      case 'L': return kKeyCode_L;
      case 'M': return kKeyCode_M;
      case 'N': return kKeyCode_N;
      case 'O': return kKeyCode_O;
      case 'P': return kKeyCode_P;
      case 'Q': return kKeyCode_Q;
      case 'R': return kKeyCode_R;
      case 'S': return kKeyCode_S;
      case 'T': return kKeyCode_T;
      case 'U': return kKeyCode_U;
      case 'V': return kKeyCode_V;
      case 'W': return kKeyCode_W;
      case 'X': return kKeyCode_X;
      case 'Y': return kKeyCode_Y;
      case 'Z': return kKeyCode_Z;
    }
    return kKeyCode_Invalid;
  }

  static LRESULT CALLBACK static_window_proc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
    Window* window = (Window*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
    if (!window) return DefWindowProc(hWnd, msg, wParam, lParam);
    switch (msg)
    {
      case WM_MOUSEMOVE:
      {
        EventFactory::push(
          *window, 
          kWindowEventType_MouseMove,
          (float)GET_X_LPARAM(lParam),
          (float)GET_Y_LPARAM(lParam));
      } break;
      case WM_LBUTTONDOWN:
      {
        EventFactory::push(
          *window,
          kWindowEventType_MouseDown,
          kMouseButton_Left);
      } break;
      case WM_LBUTTONUP:
      {
        EventFactory::push(
          *window,
          kWindowEventType_MouseUp,
          kMouseButton_Left);
      } break;
      case WM_RBUTTONDOWN:
      {
        EventFactory::push(
          *window,
          kWindowEventType_MouseDown,
          kMouseButton_Right);
      } break;
      case WM_RBUTTONUP:
      {
        EventFactory::push(
          *window,
          kWindowEventType_MouseUp,
          kMouseButton_Right);
      } break;
     
      case WM_KEYDOWN:
      {
        EventFactory::push(
          *window,
          kWindowEventType_KeyDown,
          convert(wParam));
      } break;
      case WM_KEYUP:
      {
        EventFactory::push(
          *window,
          kWindowEventType_KeyUp,
          convert(wParam));
      } break;

      case WM_UNICHAR:
      case WM_CHAR:
      {
        EventFactory::push(
          *window,
          kWindowEventType_Text,
          (unsigned)wParam);
      } break;

      case WM_CLOSE:
      {
        EventFactory::push(*window, kWindowEventType_Quit);
      } break;
      default:
      {
        return DefWindowProc(hWnd, msg, wParam, lParam);
      } break;
    }
    return 0;
  }

  Window::Window()
  {
    m_handle = nullptr;
    m_width = 0;
    m_height = 0;
  }

  Window::~Window()
  {
    close();
  }

  bool Window::open(int width, int height, const char* title)
  {
    m_width = width;
    m_height = height;
    WNDCLASSEX wc = { 0 };
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc = static_window_proc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = GetModuleHandle(NULL);
    //wc.hIcon = ::LoadIcon(NULL, IDI_SHIELD);
    //wc.hIconSm = ::LoadIcon(NULL, IDI_SHIELD);
    wc.hCursor = ::LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)::GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = L"cottonWindowClass";
    if (!RegisterClassEx(&wc)) return false;
    int style = (WS_OVERLAPPEDWINDOW & ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));
    RECT rc = { 0, 0, width, height };
    AdjustWindowRect(&rc, style, FALSE);
    RECT deskrect;
    HWND desktop = GetDesktopWindow();
    GetWindowRect(desktop, &deskrect);
    int centerx = deskrect.right / 2 - ((rc.right - rc.left) / 2);
    int centery = deskrect.bottom / 2 - ((rc.bottom - rc.top) / 2);
    m_handle = CreateWindowEx(
      0,
      wc.lpszClassName, 
      L"cotton", 
      style,
      centerx, centery,
      rc.right - rc.left, 
      rc.bottom - rc.top,
      NULL, 
      NULL, 
      wc.hInstance, 
      NULL);
    if (!m_handle) return false;
    ShowWindow((HWND)m_handle, SW_SHOWNORMAL);
    if (title) SetWindowTextA((HWND)m_handle, title);
    SetWindowLongPtr((HWND)m_handle, GWLP_USERDATA, (LONG_PTR)this);
    HDC dc = GetDC((HWND)m_handle);
    PIXELFORMATDESCRIPTOR dpfd = { 0 };
    dpfd.nSize = sizeof(dpfd);
    dpfd.nVersion = 1;
    dpfd.iPixelType = PFD_TYPE_RGBA;
    dpfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
    dpfd.cColorBits = 32;
    dpfd.cAlphaBits = 8;
    dpfd.iLayerType = PFD_MAIN_PLANE;
    int index = ChoosePixelFormat(dc, &dpfd);
    PIXELFORMATDESCRIPTOR spfd = { 0 };
    DescribePixelFormat(dc, index, sizeof(spfd), &spfd);
    SetPixelFormat(dc, index, &spfd);
    m_context = wglCreateContext(dc);
    wglMakeCurrent(dc, (HGLRC)m_context);
    ReleaseDC((HWND)m_handle, dc);

    // note(tommi): get all extensions
    const GLubyte* ext = glGetString(GL_EXTENSIONS);
    if (ext)
    {
      // note(tommi): look for windows swap control
      if (strstr((const char*)ext, "WGL_EXT_swap_control") != 0)
      {
        typedef void(*PFNWGLSWAPINTERVALEXTPROC)(GLint);
        PFNWGLSWAPINTERVALEXTPROC wglSwapInterval = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
        // note(tommi): success, turn v-sync on
        if (wglSwapInterval) wglSwapInterval(1);
      }
    }

    glViewport(0, 0, m_width, m_height);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glClearColor(0.1f, 0.15f, 0.2f, 1.0f);
    glDisable(GL_CULL_FACE);
    glEnable(GL_SCISSOR_TEST);
    return true;
  }

  void Window::close()
  {
    if (m_context)
    {
      wglMakeCurrent(nullptr, nullptr);
      wglDeleteContext((HGLRC)m_context);
      m_context = nullptr;
    }
    if (m_handle)
    {
      CloseWindow((HWND)m_handle);
      m_handle = nullptr;
    }
    m_width = 0;
    m_height = 0;
    m_queue.clear();
  }

  bool Window::is_open() const
  {
    if (!m_handle) return false;
    MSG msg = { 0 };
    while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
    return true;
  }

  void Window::set_title(const char* title)
  {
    if (!m_handle || !title) return;
    SetWindowTextA((HWND)m_handle, title);
  }

  int Window::get_width() const
  {
    return m_width;
  }

  int Window::get_height() const
  {
    return m_height;
  }

  void* Window::get_native_handle() const
  {
    return m_handle;
  }

  bool Window::poll_event(WindowEvent& event)
  {
    if (m_queue.empty()) return false;
    event = m_queue.front();
    m_queue.pop_front();
    return true;
  }

  void Window::push_event(const WindowEvent& event)
  {
    m_queue.push_back(event);
  }

  void Window::clear()
  {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLdouble)m_width, (GLdouble)m_height, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
  }

  void Window::present()
  {
    glFlush();
    glFinish();
    if (!m_handle) return;
    HDC dc = GetDC((HWND)m_handle);
    SwapBuffers(dc);
  }

  void Window::draw(const Transform& transform, const Vertex* vertices, const unsigned num_vertices, const Texture* texture) const
  {
    static unsigned current_id = 0;
    if (!vertices || num_vertices == 0) return;

    const char* data = (const char*)vertices;
    glVertexPointer(2, GL_FLOAT, sizeof(Vertex), data + 0);
    glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), data + 8);
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), data + 16);

    if (texture && texture->is_valid())
    {
      if (current_id != texture->get_handle())
      {
        current_id = texture->get_handle();
        glBindTexture(GL_TEXTURE_2D, current_id);
      }
    }
    else
    {
      if (current_id != 0)
        glBindTexture(GL_TEXTURE_2D, current_id = 0);
    }
    // todo(tommi): handle transform proper
    const Vector2& position = transform.get_position();
    const Vector2& scale = transform.get_scale();
    const Vector2& origin = transform.get_origin();
    glPushMatrix();
    glTranslatef(position.m_x, position.m_y, 0.0f);
    glScalef(scale.m_x, scale.m_y, 1.0f);
    glRotatef(transform.get_rotation(), 0.0f, 0.0f, 1.0f);
    glTranslatef(-origin.m_x, -origin.m_y, 0.0f);
    glDrawArrays(GL_QUADS, 0, num_vertices);
    glPopMatrix();
  }

  void Window::set_clip(const Rect& clip) const
  {
    Rect rect = clip;
    rect.m_w = rect.m_w - rect.m_x;
    rect.m_h = rect.m_h - rect.m_y;
    glScissor(rect.m_x, m_height - rect.m_y - rect.m_h, rect.m_w, rect.m_h);
  }

  void Window::reset_clip() const
  {
    glScissor(0, 0, m_width, m_height);
  }

  Vertex::Vertex()
    : m_position(0.0f, 0.0f)
    , m_texcoord(0.0f, 0.0f)
    , m_color(0xffffffff)
  {
  }

  Vertex::Vertex(const Vector2& position, const Vector2& texcoord, const unsigned color)
    : m_position(position)
    , m_texcoord(texcoord)
    , m_color(color)
  {
  }

  Transform::Transform()
    : m_position(0.0f, 0.0f)
    , m_scale(1.0f, 1.0f)
    , m_rotation(0.0f)
  {
  }

  void Transform::set_origin(const Vector2& origin)
  {
    m_origin = origin;
  }

  void Transform::set_origin(float x, float y)
  {
    m_origin.m_x = x;
    m_origin.m_y = y;
  }

  void Transform::set_position(const Vector2& position)
  {
    m_position = position;
  }

  void Transform::set_position(float x, float y)
  {
    m_position.m_x = x;
    m_position.m_y = y;
  }

  void Transform::set_scale(const Vector2& scale)
  {
    m_scale = scale;
  }

  void Transform::set_scale(float x, float y)
  {
    m_scale.m_x = x;
    m_scale.m_y = y;
  }

  void Transform::set_rotation(float rotation)
  {
    m_rotation = rotation;
  }

  const Vector2& Transform::get_origin() const
  {
    return m_origin;
  }

  const Vector2 Transform::get_position() const
  {
    return m_position;
  }

  const Vector2& Transform::get_scale() const
  {
    return m_scale;
  }

  float Transform::get_rotation() const
  {
    return m_rotation;
  }

  Texture::Texture()
  {
    m_id = 0;
    m_width = 0;
    m_height = 0;
  }

  Texture::~Texture()
  {
  }

  bool Texture::create_from_filename(const char* filename)
  {
    if (is_valid()) destroy();
    FILE* fin = nullptr;
    fopen_s(&fin, filename, "rb");
    if (!fin) return false;
    int components = 0;
    stbi_uc* bitmap = stbi_load_from_file(fin, &m_width, &m_height, &components, 4);
    fclose(fin);
    if (!bitmap) return false;
    glGenTextures(1, &m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(bitmap);
    return glGetError() == GL_NO_ERROR;
  }

  bool Texture::create_from_memory(int width, int height, const void* bitmap)
  {
    m_width = width;
    m_height = height;
    glGenTextures(1, &m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap);
    glBindTexture(GL_TEXTURE_2D, 0);
    return glGetError() == GL_NO_ERROR;
  }

  void Texture::destroy()
  {
    glDeleteTextures(1, &m_id);
    m_id = 0;
  }

  bool Texture::is_valid() const
  {
    return m_id != 0;
  }

  int Texture::get_width() const
  {
    return m_width;
  }

  int Texture::get_height() const
  {
    return m_height;
  }

  unsigned Texture::get_handle() const
  {
    return m_id;
  }

  Font::Font()
  {
    m_glyphs.reserve(256);
    m_vertices.resize(4096);
    m_size = 0.0f;
  }

  Font::~Font()
  {
    destroy();
  }

  static void get_glyph_size(stbtt_fontinfo& info, unsigned codepoint, float size, int& width, int& height)
  {
    int x0, y0, x1, y1;
    float shift_x = 0;
    float shift_y = 0;
    float scale = stbtt_ScaleForMappingEmToPixels(&info, size);
    stbtt_GetCodepointBitmapBoxSubpixel(&info, codepoint, scale, scale, shift_x, shift_y, &x0, &y0, &x1, &y1);
    width = x1 - x0;
    height = y1 - y0;
  }

  static void blit(unsigned char* dst, const Rect& dst_rect, const unsigned char* src, const Rect& src_rect)
  {
    int src_width = src_rect.m_w;
    int dst_width = dst_rect.m_w;
    int dst_height = dst_rect.m_h;

    for (int y = 0; y < src_rect.m_h; y++)
    {
      int dst_y = dst_rect.m_y + y;
      if (dst_y >= dst_height || dst_y < 0)
        continue;
      dst_y *= dst_width;

      int src_y = src_rect.m_y + y;
      if (src_y >= src_rect.m_y + src_rect.m_h || src_y < 0)
        continue;
      src_y *= src_width;

      for (int x = 0; x < src_rect.m_w; x++)
      {
        int dst_x = dst_rect.m_x + x;
        if (dst_x >= dst_width || dst_x < 0)
          continue;

        int src_x = src_rect.m_x + x;
        if (src_x >= src_rect.m_x + src_rect.m_w || src_x < 0)
          continue;

        unsigned src_index = src_x + src_y;
        unsigned dst_index = dst_x + dst_y;
        dst[dst_index] = src[src_index];
      }
    }
  }

  bool Font::create_from_filename(const char* filename, float size)
  {
    // note(tommi): this probably leaks some memory
    // but I don't care since it is so awesome!
    FILE* fin = nullptr;
    fopen_s(&fin, filename, "rb");
    if (!fin) return false;
    fseek(fin, 0, SEEK_END);
    size_t fontsize = ftell(fin);
    fseek(fin, 0, SEEK_SET);
    char* fontbuf = (char*)malloc(fontsize);
    fread(fontbuf, 1, fontsize, fin);
    fclose(fin);

    m_size = size;
    const unsigned num_nodes = 256;
    stbrp_rect rects[num_nodes] = { 0 };
    stbrp_node nodes[num_nodes] = { 0 };
    stbrp_context context;
    stbrp_init_target(&context, 512, 512, nodes, num_nodes);
    stbtt_fontinfo info = { 0 };
    stbtt_InitFont(&info, (const unsigned char*)fontbuf,
      stbtt_GetFontOffsetForIndex((const unsigned char *)fontbuf, 0));
    float scale = stbtt_ScaleForPixelHeight(&info, m_size);
    float inv_x = 1.0f / 512.0f;
    float inv_y = 1.0f / 512.0f;

    for (unsigned i = 0; i < num_nodes; i++)
    {
      int width = 0, height = 0;
      get_glyph_size(info, i, size, width, height);
      rects[i].w = (stbrp_coord)width;
      rects[i].h = (stbrp_coord)height;
    }
    stbrp_pack_rects(&context, rects, num_nodes);

    int ascent = 0;
    stbtt_GetFontVMetrics(&info, &ascent, 0, 0);
    float baseline = ascent * scale;
    for (unsigned i = 0; i < num_nodes; i++)
    {
      int x0 = 0, x1 = 0, y0 = 0, y1 = 0;
      stbtt_GetCodepointBitmapBoxSubpixel(&info, i, scale, scale, 0, 0, &x0, &y0, &x1, &y1);

      int advance = 0;
      stbtt_GetCodepointHMetrics(&info, i, &advance, 0);

      Glyph glyph = { 0 };
      glyph.width = (float)rects[i].w;
      glyph.height = (float)rects[i].h;
      glyph.offset_x = advance * scale;
      glyph.offset_y = baseline + y0;
      glyph.uv[0] = rects[i].x * inv_x;
      glyph.uv[1] = rects[i].y * inv_y;
      glyph.uv[2] = rects[i].w * inv_x;
      glyph.uv[3] = rects[i].h * inv_y;
      m_glyphs.push_back(glyph);
    }

    unsigned char temp[512 * 512] = { 0 };
    for (unsigned i = 0; i < num_nodes; i++)
    {
      int width = 0, height = 0, offx = 0, offy = 0;
      unsigned char* src = stbtt_GetCodepointBitmap(&info, 0, scale, i, &width, &height, &offx, &offy);
      blit(temp, Rect(rects[i].x, rects[i].y, 512, 512), src, Rect(0, 0, width, height));
      stbtt_FreeBitmap(src, 0);
    }

    const unsigned count = 512 * 512;
    unsigned char* bitmap = new unsigned char[count * 4];
    for (unsigned i = 0, k = 0; i < count; i++)
    {
      bitmap[k++] = 0xff;
      bitmap[k++] = 0xff;
      bitmap[k++] = 0xff;
      bitmap[k++] = temp[i];
    }
    m_texture.create_from_memory(512, 512, bitmap);

    delete[] bitmap;
    free(fontbuf);

    return true;
  }

  void Font::destroy()
  {
    m_glyphs.clear();
    m_vertices.clear();
    m_texture.destroy();
  }

  void fill(std::vector<Vertex>& vertices, unsigned& offset, const Font::Glyph& glyph, unsigned color, float x, float y)
  {
    vertices[offset].m_position = Vector2(x, y + glyph.offset_y);
    vertices[offset].m_texcoord = Vector2(glyph.uv[0], glyph.uv[1]);
    vertices[offset].m_color = color;
    offset++;

    vertices[offset].m_position = Vector2(x + glyph.width, y + glyph.offset_y);
    vertices[offset].m_texcoord = Vector2(glyph.uv[0] + glyph.uv[2], glyph.uv[1]);
    vertices[offset].m_color = color;
    offset++;

    vertices[offset].m_position = Vector2(x + glyph.width, y + glyph.height + glyph.offset_y);
    vertices[offset].m_texcoord = Vector2(glyph.uv[0] + glyph.uv[2], glyph.uv[1] + glyph.uv[3]);
    vertices[offset].m_color = color;
    offset++;

    vertices[offset].m_position = Vector2(x, y + glyph.height + glyph.offset_y);
    vertices[offset].m_texcoord = Vector2(glyph.uv[0], glyph.uv[1] + glyph.uv[3]);
    vertices[offset].m_color = color;
    offset++;
  }

  void Font::draw(const Window& window, int x, int y, unsigned color, const std::string& text)
  {
    unsigned offset = 0;
    int org_x = x;
    for (unsigned i = 0; i < text.length(); i++)
    {
      unsigned char ch = text[i];
      switch (ch)
      {
        case ' ':
        {
          x += (int)(m_size * 0.35f + 0.5f);
        } break;
        case '\n':
        {
          x = org_x;
          y += (int)(m_size + 0.5f);
        } break;
        default:
        {
          Glyph& glyph = m_glyphs[(unsigned)ch];
          fill(m_vertices, offset, glyph, color, (float)x, (float)y);
          x += (int)(glyph.width + 0.5f);
        } break;
      }
    }

    static Transform transform;
    window.draw(transform, &m_vertices[0], offset, &m_texture);
  }

  // note(tommi): audio
   static LONG to_log(float volume)
  {
    LONG v = DSBVOLUME_MIN;
    if (volume >= 1.0f)
      return DSBVOLUME_MAX;
    v = (LONG)(-2000.0f * logf(1.0f / volume));
    return Math::clamp(v, (LONG)DSBVOLUME_MIN, (LONG)DSBVOLUME_MAX);
  }

  class AudioFactory
  {
  public:
    static Sound::Impl* create_sound(Audio& audio, const char* filename);
    static Music::Impl* create_music(Audio& audio, const char* filename);
  };

  class AudioBufferFactory
  {
  public:
    static IDirectSoundBuffer8* create_buffer(Audio& audio, const short* pcm, unsigned size);
    static IDirectSoundBuffer8* create_buffer(Audio& audio, unsigned seconds);
  };

  class AudioPlayer
  {
  public:
    static void play(Audio& audio, IDirectSoundBuffer* buffer, float volume);
    static void play(Audio& audio, Music* music);
    static void stop(Audio& audio, Music* music);
  };

  class Audio::Impl
  {
    friend class AudioFactory;
    friend class AudioBufferFactory;
    friend class AudioPlayer;

  public:
    Impl()
    {
      m_device = nullptr;
      m_primary = nullptr;
    }

    ~Impl()
    {
      close();
    }

    bool open(const Window& window)
    {
      if (is_valid()) return true;
      DirectSoundCreate(0, &m_device, 0);
      m_device->SetCooperativeLevel((HWND)window.get_native_handle(), DSSCL_PRIORITY);
      DSBUFFERDESC desc = { 0 };
      desc.dwSize = sizeof(DSBUFFERDESC);
      desc.dwFlags = DSBCAPS_PRIMARYBUFFER;
      m_device->CreateSoundBuffer(&desc, &m_primary, 0);
      WAVEFORMATEX format = { 0 };
      format.wFormatTag = WAVE_FORMAT_PCM;
      format.nChannels = 2;
      format.nSamplesPerSec = 44100;
      format.wBitsPerSample = 16;
      format.nBlockAlign = (format.nChannels * format.wBitsPerSample) / 8;
      format.nAvgBytesPerSec = format.nSamplesPerSec * format.nBlockAlign;
      format.cbSize = 0;
      m_primary->SetFormat(&format);
      return true;
    }

    void close()
    {
      if (!m_duplicates.empty())
      {
        auto itr = m_duplicates.begin();
        while (itr != m_duplicates.end())
        {
          (*itr)->Stop();
          (*itr)->Release();
          ++itr;
        }
        m_duplicates.clear();
      }
      if (!m_music.empty())
      {
        auto itr = m_music.begin();
        while (itr != m_music.end())
        {
          // note(tommi): do we really need to do this?
          //(*itr)->stop();
          ++itr;
        }
        m_music.clear();
      }
      if (m_primary)
      {
        m_primary->Release();
        m_primary = nullptr;
      }
      if (m_device)
      {
        m_device->Release();
        m_device = nullptr;
      }
    }

    void present()
    {
      // todo(tommi): handle music
      if (!m_duplicates.empty())
      {
        auto itr = m_duplicates.begin();
        while (itr != m_duplicates.end())
        {
          DWORD status = 0;
          (*itr)->GetStatus(&status);
          if (status != DSBSTATUS_PLAYING)
          {
            (*itr)->Release();
            itr = m_duplicates.erase(itr);
          }
          else
          {
            ++itr;
          }
        }
      }

      if (!m_music.empty())
      {
        auto itr = m_music.begin();
        while (itr != m_music.end())
        {
          (*itr)->update();
          ++itr;
        }
      }
    }

    bool is_valid() const
    {
      return m_device != nullptr;
    }

  private:
    IDirectSound* m_device;
    IDirectSoundBuffer* m_primary;
    std::list<IDirectSoundBuffer*> m_duplicates;
    std::list<Music*> m_music;
  };

  Audio::Audio()
  {
    m_pimpl = new Impl;
  }

  Audio::~Audio()
  {
    delete m_pimpl;
  }

  bool Audio::open(const Window& window)
  {
    return m_pimpl->open(window);
  }

  void Audio::close()
  {
    m_pimpl->close();
  }

  void Audio::present()
  {
    m_pimpl->present();
  }

  class Sound::Impl
  {
    friend class AudioPlayer;

  public:
    Impl(Audio& audio)
      : m_audio(audio)
    {
      m_secondary = nullptr;
    }

    ~Impl()
    {
      destroy();
    }

    bool create(const char* filename)
    {
      short* pcm = nullptr;
      int channels = 0, sample_rate = 0;
      int num_samples = stb_vorbis_decode_filename(filename, &channels, &sample_rate, &pcm);
      int size = num_samples * channels * sizeof(short);
      if (!pcm) return false;
      m_secondary = AudioBufferFactory::create_buffer(m_audio, pcm, size); 
      free(pcm);
      return true;
    }

    void destroy()
    {
      if (m_secondary)
      {
        m_secondary->Release();
        m_secondary = nullptr;
      }
    }

    bool is_valid() const
    {
      return m_secondary != nullptr;
    }

    void play(float volume)
    {
      AudioPlayer::play(m_audio, m_secondary, volume);
    }

  private:
    Audio& m_audio;
    IDirectSoundBuffer8* m_secondary;
  };

  Sound::Sound(Audio& audio, const char* filename)
  {
    m_pimpl = AudioFactory::create_sound(audio, filename);
  }

  Sound::~Sound()
  {
    delete m_pimpl;
  }

  void Sound::destroy()
  {
    m_pimpl->destroy();
  }

  void Sound::play(float volume)
  {
    m_pimpl->play(volume);
  }

  class Music::Impl
  {
    friend class AudioPlayer;
    friend class Music;

  public:
    Impl(Audio& audio)
      : m_audio(audio)
    {
      m_vorbis = nullptr;
      m_secondary = nullptr;
      m_last_chunk = 0;
      m_num_chunks = 0;
    }

    ~Impl()
    {
      destroy();
    }

    bool create(const char* filename)
    {
      const unsigned seconds = 4;
      m_secondary = AudioBufferFactory::create_buffer(m_audio, seconds);
      if (!m_secondary) return false;
      //HRESULT hr = m_secondary->Play(0, 0, DSBPLAY_LOOPING); (void)hr;
      m_vorbis = stb_vorbis_open_filename(filename, 0, 0);
      if (!m_vorbis) return false;
      m_info = stb_vorbis_get_info(m_vorbis);
      m_num_chunks = (m_info.sample_rate * m_info.channels * seconds) / ArraySize(m_frame);
      return true;
    }

    void destroy()
    {
      if (m_secondary)
      {
        m_secondary->Release();
        m_secondary = nullptr;
      }
      if (m_vorbis)
      {
        stb_vorbis_close(m_vorbis);
        m_vorbis = nullptr;
      }
    }

    bool is_valid() const
    {
      return m_vorbis != nullptr && m_secondary != nullptr;
    }

    void update()
    {
      if (!m_vorbis || !m_secondary || !is_playing()) return;

      DWORD junk = 0, cursor = 0;
      HRESULT hr = m_secondary->GetCurrentPosition(&junk, &cursor);
      if (SUCCEEDED(hr))
      {
        unsigned this_chunk = m_last_chunk;
        cursor /= sizeof(m_frame);
        m_last_chunk = cursor;
        if (m_last_chunk != this_chunk)
        {
          cursor = (cursor + 1) % m_num_chunks;
          cursor *= sizeof(m_frame);
          memset(m_frame, 0, sizeof(m_frame));
          int bytes = 0, array_size = ArraySize(m_frame);
          while (bytes < sizeof(m_frame))
          {
            int result = stb_vorbis_get_samples_short_interleaved(m_vorbis, m_info.channels, m_frame + bytes, array_size - bytes);
            if (result == 0)
              break;
            bytes += result * m_info.channels;
          }
          if (bytes > 0)
          {
            DWORD locked_size0 = 0, locked_size1 = 0;
            void* locked_buffer0 = 0, *locked_buffer1 = 0;
            hr = m_secondary->Lock(cursor, sizeof(m_frame), &locked_buffer0, &locked_size0, &locked_buffer1, &locked_size1, 0);
            if (SUCCEEDED(hr))
            {
              memcpy(locked_buffer0, m_frame, sizeof(m_frame));
              if (locked_buffer1)
              {
                memcpy(locked_buffer1, m_frame + locked_size0, locked_size1);
              }
              m_secondary->Unlock(locked_buffer0, locked_size0, locked_buffer1, locked_size1);
            }
          }
          else if (bytes == 0)
          {
            m_secondary->Stop();
          }
        }
      }
    }

    void play(float volume)
    {
      if (!m_secondary) return;
      if (is_playing()) return;
      m_secondary->SetVolume(to_log(volume));
      m_secondary->Play(0, 0, DSBPLAY_LOOPING);
    }

    void pause()
    {
      if (!m_secondary) return;
      m_secondary->Stop();
    }

    void stop()
    {
      if (m_vorbis) stb_vorbis_seek_start(m_vorbis);
      if (!m_secondary) return;
      m_secondary->Stop();
      m_secondary->SetCurrentPosition(0);
    }

    bool is_playing() const
    {
      if (!m_secondary) return false;
      DWORD status = 0;
      m_secondary->GetStatus(&status);
      return (status & DSBSTATUS_PLAYING) > 0;
    }

    void set_volume(float volume)
    {
      if (!m_secondary) return;
      m_secondary->SetVolume(to_log(volume));
    }

  private:
    Audio& m_audio;
    stb_vorbis_info m_info;
    stb_vorbis* m_vorbis;
    IDirectSoundBuffer8* m_secondary;
    unsigned m_last_chunk;
    unsigned m_num_chunks;
    short m_frame[22050];
  };

  Music::Music(Audio& audio, const char* filename)
  {
    m_pimpl = AudioFactory::create_music(audio, filename);
  }

  Music::~Music()
  {
    delete m_pimpl;
  }

  void Music::destroy()
  {
    m_pimpl->destroy();
  }

  void Music::update()
  {
    m_pimpl->update();
  }

  void Music::play(float volume)
  {
    m_pimpl->play(volume);
    AudioPlayer::play(m_pimpl->m_audio, this);
  }

  void Music::pause()
  {
    m_pimpl->pause();
    AudioPlayer::stop(m_pimpl->m_audio, this);
  }

  void Music::stop()
  {
    m_pimpl->stop();
    AudioPlayer::stop(m_pimpl->m_audio, this);
  }

  bool Music::is_playing() const
  {
    return m_pimpl->is_playing();
  }

  void Music::set_volume(float volume)
  {
    m_pimpl->set_volume(volume);
  }

  // note(tommi): internal factory and helper classes
  Sound::Impl* AudioFactory::create_sound(Audio& audio, const char* filename)
  {
    Sound::Impl* result = new Sound::Impl(audio);
    result->create(filename);
    return result;
  }

  Music::Impl* AudioFactory::create_music(Audio& audio, const char* filename)
  {
    Music::Impl* result = new Music::Impl(audio);
    result->create(filename);
    return result;
  }

  IDirectSoundBuffer8* AudioBufferFactory::create_buffer(Audio& audio, const short* pcm, unsigned size)
  {
    WAVEFORMATEX wave_format;
    wave_format.wFormatTag = WAVE_FORMAT_PCM;
    wave_format.nSamplesPerSec = 44100;
    wave_format.wBitsPerSample = 16;
    wave_format.nChannels = 2;
    wave_format.nBlockAlign = (wave_format.wBitsPerSample / 8) * wave_format.nChannels;
    wave_format.nAvgBytesPerSec = wave_format.nSamplesPerSec * wave_format.nBlockAlign;
    wave_format.cbSize = 0;

    DSBUFFERDESC buffer_desc;
    buffer_desc.dwSize = sizeof(DSBUFFERDESC);
    buffer_desc.dwFlags = DSBCAPS_CTRLVOLUME;
    buffer_desc.dwBufferBytes = size;
    buffer_desc.dwReserved = 0;
    buffer_desc.lpwfxFormat = &wave_format;
    buffer_desc.guid3DAlgorithm = GUID_NULL;

    HRESULT hr = S_OK;
    IDirectSoundBuffer* temp_buffer = nullptr;
    IDirectSoundBuffer8* result = nullptr;
    hr = audio.m_pimpl->m_device->CreateSoundBuffer(&buffer_desc, &temp_buffer, NULL);
    assert(SUCCEEDED(hr));
    hr = temp_buffer->QueryInterface(IID_IDirectSoundBuffer, (void**)&result);
    assert(SUCCEEDED(hr));
    temp_buffer->Release();
    temp_buffer = nullptr;
    if (pcm)
    {
      char* buffer_ptr = nullptr;
      DWORD buffer_size = 0;
      hr = result->Lock(0, size, (void**)&buffer_ptr, (LPDWORD)&buffer_size, NULL, 0, 0);
      assert(SUCCEEDED(hr));
      memcpy(buffer_ptr, pcm, size);
      hr = result->Unlock((void**)buffer_ptr, buffer_size, NULL, 0);
      assert(SUCCEEDED(hr));
    }
    return result;
  }

  IDirectSoundBuffer8* AudioBufferFactory::create_buffer(Audio& audio, unsigned seconds)
  {
    WAVEFORMATEX wave_format;
    wave_format.wFormatTag = WAVE_FORMAT_PCM;
    wave_format.nSamplesPerSec = 44100;
    wave_format.wBitsPerSample = 16;
    wave_format.nChannels = 2;
    wave_format.nBlockAlign = (wave_format.wBitsPerSample / 8) * wave_format.nChannels;
    wave_format.nAvgBytesPerSec = wave_format.nSamplesPerSec * wave_format.nBlockAlign;
    wave_format.cbSize = 0;

    DSBUFFERDESC buffer_desc;
    buffer_desc.dwSize = sizeof(DSBUFFERDESC);
    buffer_desc.dwFlags = DSBCAPS_CTRLVOLUME;
    buffer_desc.dwBufferBytes = wave_format.nAvgBytesPerSec * seconds;
    buffer_desc.dwReserved = 0;
    buffer_desc.lpwfxFormat = &wave_format;
    buffer_desc.guid3DAlgorithm = GUID_NULL;

    HRESULT hr = S_OK;
    IDirectSoundBuffer* temp_buffer = nullptr;
    IDirectSoundBuffer8* result = nullptr;
    hr = audio.m_pimpl->m_device->CreateSoundBuffer(&buffer_desc, &temp_buffer, NULL);
    assert(SUCCEEDED(hr));
    hr = temp_buffer->QueryInterface(IID_IDirectSoundBuffer, (void**)&result);
    assert(SUCCEEDED(hr));
    temp_buffer->Release();
    temp_buffer = nullptr;
    return result;
  }

  void AudioPlayer::play(Audio& audio, IDirectSoundBuffer* buffer, float volume)
  {
    IDirectSoundBuffer* duplicate = nullptr;
    audio.m_pimpl->m_device->DuplicateSoundBuffer(buffer, &duplicate);
    duplicate->SetCurrentPosition(0);
    duplicate->SetVolume(to_log(volume));
    duplicate->Play(0, 0, 0);
    audio.m_pimpl->m_duplicates.push_back(duplicate);
  }

  void AudioPlayer::play(Audio& audio, Music *music)
  {
    audio.m_pimpl->m_music.push_back(music);
  }

  void AudioPlayer::stop(Audio& audio, Music* music)
  {
    auto itr = audio.m_pimpl->m_music.begin();
    while (itr != audio.m_pimpl->m_music.end())
    {
      if ((*itr) == music)
      {
        audio.m_pimpl->m_music.erase(itr);
        break;
      }
      ++itr;
    }
  }

  Sprite::Sprite()
  {
    m_texture = nullptr;
  }

  Sprite::Sprite(const Texture* texture)
  {
    m_texture = texture;
  }

  Sprite::Sprite(const Texture* texture, const Rect& texture_rectangle)
  {
    m_texture = texture;
    update(texture_rectangle);
  }

  Sprite::~Sprite()
  {
    m_texture = nullptr;
  }

  void Sprite::set_texture(const Texture* texture)
  {
    m_texture = texture;
  }

  void Sprite::set_rectangle(const Rect& rect)
  {
    update(rect);
  }

  void Sprite::set_rectangle(int x, int y, int w, int h)
  {
    update(Rect(x, y, w, h));
  }

  void Sprite::set_color(unsigned color)
  {
    m_vertices[0].m_color = color;
    m_vertices[1].m_color = color;
    m_vertices[2].m_color = color;
    m_vertices[3].m_color = color;
  }

  unsigned Sprite::get_vertex_count() const
  {
    return ArraySize(m_vertices);
  }

  const Vertex* Sprite::get_vertices() const
  {
    return m_vertices;
  }

  const Texture* Sprite::get_texture() const
  {
    return m_texture;
  }

  void Sprite::update(const Rect& rect)
  {
    float inv_x = (float)1.0f / (float)m_texture->get_width();
    float inv_y = (float)1.0f / (float)m_texture->get_height();
    float tx0 = inv_x * (float)rect.m_x;
    float ty0 = inv_y * (float)rect.m_y;
    float tx1 = inv_x * (float)(rect.m_x + rect.m_w);
    float ty1 = inv_y * (float)(rect.m_y + rect.m_h);
    m_vertices[0].m_texcoord = Vector2(tx0, ty0);
    m_vertices[1].m_texcoord = Vector2(tx1, ty0);
    m_vertices[2].m_texcoord = Vector2(tx1, ty1);
    m_vertices[3].m_texcoord = Vector2(tx0, ty1);

    float pw = (float)rect.m_w;
    float ph = (float)rect.m_h;
    m_vertices[0].m_position = Vector2(0.0f, 0.0f);
    m_vertices[1].m_position = Vector2(pw, 0.0f);
    m_vertices[2].m_position = Vector2(pw, ph);
    m_vertices[3].m_position = Vector2(0.0f, ph);
  }

  SpriteBatch::SpriteBatch()
  {
    m_texture = nullptr;
    m_vertices.reserve(256);
  }

  SpriteBatch::SpriteBatch(const Texture* texture)
  {
    m_texture = texture;
    m_vertices.reserve(256);
  }

  SpriteBatch::SpriteBatch(const Texture* texture, unsigned num_vertices)
  {
    m_texture = texture;
    m_vertices.reserve(num_vertices);
  }

  SpriteBatch::~SpriteBatch()
  {
    m_vertices.clear();
  }

  void SpriteBatch::set_texture(const Texture* texture)
  {
    m_texture = texture;
  }

  bool SpriteBatch::is_valid() const
  {
    return !m_vertices.empty();
  }

  unsigned SpriteBatch::get_vertex_count() const
  {
    return (unsigned)m_vertices.size();
  }

  const Vertex* SpriteBatch::get_vertices() const
  {
    if (m_vertices.empty()) return nullptr;
    return &m_vertices[0];
  }

  const Texture* SpriteBatch::get_texture() const
  {
    return m_texture;
  }

  void SpriteBatch::push(const Rect& p, const Rect& t, unsigned color)
  {
    float inv_x = 0.0f; 
    float inv_y = 0.0f; 
    if (m_texture)
    {
      inv_x = (float)1.0f / (float)m_texture->get_width();
      inv_y = (float)1.0f / (float)m_texture->get_height();
    }

    float tx0 = inv_x * (float)t.m_x;
    float ty0 = inv_y * (float)t.m_y;
    float tx1 = inv_x * (float)(t.m_x + t.m_w);
    float ty1 = inv_y * (float)(t.m_y + t.m_h);

    Vertex vertices[4] =
    {
      Vertex(Vector2(p.m_x,         p.m_y),         Vector2(tx0, ty0), color),
      Vertex(Vector2(p.m_x + p.m_w, p.m_y),         Vector2(tx1, ty0), color),
      Vertex(Vector2(p.m_x + p.m_w, p.m_y + p.m_h), Vector2(tx1, ty1), color),
      Vertex(Vector2(p.m_x,         p.m_y + p.m_h), Vector2(tx0, ty1), color),
    };

    for (unsigned i = 0; i < 4; i++)
    {
      m_vertices.push_back(vertices[i]);
    }
  }

  void SpriteBatch::reset()
  {
    m_vertices.clear();
  }

  void Entity::update(float deltatime)
  {
    (void)deltatime;
  }

  void Entity::draw(const Window& window)
  {
    window.draw(m_transform, m_sprite.get_vertices(), m_sprite.get_vertex_count(), m_sprite.get_texture());
  }

  void Entity::set_layer_bits(unsigned layer_bits)
  {
    m_layer_bits = layer_bits;
  }

  unsigned Entity::get_layer_bits() const
  {
    return m_layer_bits;
  }

  const Transform& Entity::get_transform() const
  {
    return m_transform;
  }

  Transform& Entity::get_transform()
  {
    return m_transform;
  }

  const Sprite& Entity::get_sprite() const
  {
    return m_sprite;
  }

  Sprite& Entity::get_sprite()
  {
    return m_sprite;
  }

  Vector2 Entity::get_dimensions() const
  {
    return m_sprite.get_vertices()[2].m_position;
  }

  EntitySystem::EntitySystem()
  {
    m_entities.reserve(512);
  }

  EntitySystem::~EntitySystem()
  {
  }

  void EntitySystem::update(float deltatime)
  {
    for (unsigned i = 0; i < m_entities.size(); i++)
    {
      m_entities[i]->update(deltatime);
    }
  }

  void EntitySystem::draw(const Window& window)
  {
    for (unsigned i = 0; i < m_entities.size(); i++)
    {
      m_entities[i]->draw(window);
    }
  }

  void EntitySystem::attach_entity(Entity* entity)
  {
    m_entities.push_back(entity);
  }

  void EntitySystem::detach_entity(Entity* entity)
  {
    auto itr = m_entities.begin();
    while (itr != m_entities.end())
    {
      if ((*itr) == entity)
      {
        m_entities.erase(itr);
        break;
      }
      ++itr;
    }
  }

  unsigned EntitySystem::query_in_range(QueryResult& result, const Vector2& center, const float range, const unsigned layer) const
  {
    for (unsigned i = 0; i < m_entities.size(); i++)
    {
      const Entity* entity = m_entities[i];
      if ((entity->get_layer_bits() & layer) > 0)
      {
        const Transform& transform = entity->get_transform();
        const Vector2 position = transform.get_position() + transform.get_origin();
        if (distance(center, position) <= range)
        {
          result.push_back(entity);
        }
      }
    }
    return (unsigned)result.size();
  }

  Camera::Camera(const Window& window)
    : m_window(window)
    , m_position(0.0f, 0.0f)
    , m_bounds(INT_MIN, INT_MIN, INT_MAX, INT_MAX)
  {
  }

  Camera::Camera(const Window& window, const Rect& bounds)
    : m_window(window) 
    , m_position(0.0f, 0.0f)
    , m_bounds(bounds)
  {
  }

  Camera::Camera(const Window& window, const Rect& bounds, const Vector2& position)
    : m_window(window)
    , m_position(position)
    , m_bounds(bounds)
  {
  }

  Camera::Camera(const Window& window, const Vector2& position)
    : m_window(window)
    , m_position(position)
    , m_bounds(INT_MIN, INT_MIN, INT_MAX, INT_MAX)
  {
  }

  void Camera::set_bounds(const Rect& bounds)
  {
    m_bounds = bounds;
    check_bounds();
  }

  const Rect& Camera::get_bounds() const
  {
    return m_bounds;
  }

  void Camera::move(const Vector2& amount)
  {
    m_position += amount;
    check_bounds();
  }

  void Camera::set_position(const Vector2& position)
  {
    m_position = position;
    check_bounds();
  }

  const Vector2& Camera::get_position() const
  {
    return m_position;
  }

  Transform Camera::transform(const Transform& t) const
  {
    Transform result(t);
    result.set_position(t.get_position() - m_position);
    return result;
  }

  void Camera::check_bounds()
  {
    if (m_position.m_x < m_bounds.m_x)
      m_position.m_x = (float)(m_bounds.m_x);
    if (m_position.m_x > m_bounds.m_w)
      m_position.m_x = (float)(m_bounds.m_w);

    if (m_position.m_y < m_bounds.m_y)
      m_position.m_y = (float)(m_bounds.m_y);
    if (m_position.m_y > m_bounds.m_h)
      m_position.m_y = (float)(m_bounds.m_h);
  }










  namespace gui
  {
    struct
    {
      const Window* window;
      SpriteBatch batch;
      struct { int x, y; bool down; } mouse;
      struct { int over; int down; } state;
      struct
      {
        struct
        {
          unsigned normal;
          unsigned over;
          unsigned down;
        } button;
      } style;
    } state_;

    namespace
    {
      bool inside(Rect r, int x, int y)
      {
        if (x <= r.m_x || x >= r.m_w) return false;
        if (y <= r.m_y || y >= r.m_h) return false;
        return true;
      }

      void push(Rect r, unsigned color)
      {
        state_.batch.push(r, Rect(1, 1, 1, 1), color);
      }
    }

    void initialize(const Window& window, const Texture* texture)
    {
      state_.window = &window;
      state_.batch.set_texture(texture);

      state_.style.button.normal = utility::to_color(0x11, 0x22, 0x33, 0xff);
      state_.style.button.over = utility::to_color(0x44, 0x55, 0x88, 0xff);
      state_.style.button.down = utility::to_color(0x66, 0x22, 0x22, 0xff);
    }

    void on_event(const WindowEvent& event)
    {
      switch (event.get_type())
      {
        case kWindowEventType_MouseMove: { state_.mouse.x = (int)event.get_x(); state_.mouse.y = (int)event.get_y(); } break;
        case kWindowEventType_MouseDown: { state_.mouse.down = event.get_button() == kMouseButton_Left ? true : false; } break;
        case kWindowEventType_MouseUp: { state_.mouse.down = false; } break;
      }
    }

    void prepare()
    {
      state_.state.over = 0;
      state_.batch.reset();
    }

    void flush()
    {
      static const Transform transform;
      state_.window->draw(transform, state_.batch.get_vertices(), state_.batch.get_vertex_count(), state_.batch.get_texture());

      if (!state_.mouse.down)
      {
        state_.state.down = 0;
      }
      else
      {
        if (!state_.state.down)
        {
          state_.state.down = -1;
        }
      }
    }

    void label(int id, int x, int y, const char* text)
    {
      (void)id; (void)x; (void)y; (void)text;
    }

    bool button(int id, int x, int y, int width, int height, const char* text)
    {
      unsigned color = state_.style.button.normal;
      Rect rect(x, y, x + width, y + height);
      if (inside(rect, state_.mouse.x, state_.mouse.y))
      {
        state_.state.over = id;
        color = state_.style.button.over;
        if (state_.mouse.down)
        {
          color = state_.style.button.down;
          if (!state_.state.down)
          {
            state_.state.down = id;
          }
        }
      }

      push(Rect(x, y, width, height), color);

      size_t len = strlen(text);
      for (size_t i = 0; i < len; i++)
      {
        // push(..., ...);
      }

      if (state_.state.down == id && state_.state.over == id && !state_.mouse.down)
        return true;

      return false;
    }
  }
}

#if defined(NDEBUG)
extern int main(int argc, char** argv);

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  return main(__argc, __argv);
}
#endif
