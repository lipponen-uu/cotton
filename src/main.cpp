// main.cpp

#include "cotton.hpp"
#include "Example.h"
#define GUIID (__LINE__)

using namespace cotton;

int main(int argc, char** argv)
{

  Config config(argc, argv);
  
  Window window;
  if (!window.open(config.m_width, config.m_height, "cotton")) return -1;

  Audio audio;
  if (!audio.open(window)) return -2;

  Clock clock;
  float accumulated = 0.0f;

  Texture gui_texture;
  gui_texture.create_from_filename("assets/gui.png");
  gui::initialize(window, &gui_texture);

  Mouse mouse;
  Keyboard keyboard;

  const int num_widths = 1;
  const int num_heights = 2;
  Camera camera(window);
  {
    int bounds_width = window.get_width() * num_widths - window.get_width();
    int bounds_height = window.get_height() * num_heights - window.get_height();
    Rect bounds(0, 0, bounds_width, bounds_height);
    camera.set_bounds(bounds);
  }

  Font font;
  font.create_from_filename("../assets/FiraMono.ttf", 32.0f);
 
  Texture texture;
  texture.create_from_filename("../assets/minecraft.png");

  Terrain terrain(&texture, window.get_width(), window.get_height());
  
  Sound* sound = new Sound(audio, "../assets/sheep.ogg");
 
  EntitySystem entities;

  Boid* boids[100];
  for (int i = 0; i < 10; i++)
  {
	  for (int j = 0; j < 10; j++)
	  {
		  int index = j * 10 + i;
		  boids[index] = new Boid(&camera, &texture,0,5*32,32,32,i*32.0f,j*32.0f);
		  entities.attach_entity(boids[index]);
	  }
  }

  bool running = true;
  while (running && window.is_open())
  {
    Time deltatime = clock.get_elapsed_time();
    clock.reset();

    accumulated += deltatime.as_seconds();

    std::string title = utility::to_string(
      "cotton - fps: %d time: %ds",
      (int)(1.0f / deltatime.as_seconds()), 
      (int)accumulated);
    window.set_title(title.c_str());

    WindowEvent event;
    while (window.poll_event(event))
    {  
      mouse.on_event(event);
      keyboard.on_event(event);
      gui::on_event(event);

      switch (event.get_type())
      {
        case kWindowEventType_Quit:
        {
          running = false;
        } break;
      }
    }
    if (mouse.is_down(kMouseButton_Left))
    {
      camera.move(-mouse.get_cursor_delta());
    }

    gui::prepare();
    if (gui::button(GUIID, 10, 10, 200, 40, "Button"))
    {
      printf("clickety!\n");
	  sound->play(1.0f);
    }

    window.clear();
    terrain.draw(camera, window);
	entities.draw(window);
	font.draw(window, 100, 100, 0xffffffff, "Hello World!");
    gui::flush();
    window.present();

    audio.present();
    mouse.update();
    keyboard.update();
  }

  for (int i = 0; i < 100; i++)
  {
	  delete boids[i];
  }

  delete sound;
  
  audio.close();
  window.close();


  return 0;
}
